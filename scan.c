#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <linux/filter.h>
#include <unistd.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static uint16_t local_port = 38555;
static char buff[40];
typedef uint16_t quint16; 
typedef uint32_t quint32; 
typedef uint8_t quint8; 

static quint16 ckSum(quint16 *data, quint16 len)
{
    quint32 cksum = 0;
    quint16 index = 0;
    quint16 len2 = len/2;
    while(index < len2)
    {
        cksum += data[index];
        index++;
    }
    if(len&0x1){
        cksum += (quint16)(*(((quint8*)data)+len-1))<<8;
    }
    
    while(cksum > 0xffff)
    {
        cksum = (cksum >> 16) + (cksum & 0xffff);
    }
    return (cksum == 0xffff)?cksum:~cksum;
}

static quint16 ckTcpSum(struct iphdr *ip, quint16 tcplen)
{
    quint32 cksum = 0;
    struct tcphdr *tcp = (struct tcphdr*)(ip+1);
    cksum += ip->saddr & 0xffff;
    cksum += ip->saddr >> 16;
    cksum += ip->daddr & 0xffff;
    cksum += ip->daddr >> 16;
    cksum +=0x6<<8;
    cksum += htons(tcplen);
    for(quint16 i=0;i<sizeof(struct tcphdr);i+=2){
        cksum += *(quint16*)&((char*)tcp)[i];
    }
    while (cksum>0xffff) {
        cksum = (cksum >> 16) + (cksum & 0xffff);
    }
    return (cksum == 0xffff)?cksum:~cksum;
}

static void build_syn(char *pkt, quint32 src, quint32 dst, quint16 dst_port)
{
    char buf[40] = {0x45, 0x00,0x00,0x28,0x00,0x00,0x00,0x00,0x40,0x06};
    struct iphdr *ip = (struct iphdr*)buf;
    struct tcphdr *tcp = (struct tcphdr*)(ip+1);
    ip->saddr = src;
    ip->daddr = dst;
    ip->check = ckSum((quint16*)ip, sizeof(struct iphdr));
    tcp->syn = 1;
    tcp->doff = 5;
    tcp->window = 8192;
    tcp->source = htons(local_port);
    tcp->dest = htons(dst_port);
    tcp->check = ckTcpSum(ip, 20);
    memcpy(pkt, buf, sizeof(buf));
}

static int create_ip_raw_socket(const int family)
{
    const int flag = 1;
    int fd = -1;
    struct sock_filter code[] = {
        //tcpdump tcp and dst port 38555 -dd
        { 0x28, 0, 0, 0x0000000c },
        { 0x15, 0, 4, 0x000086dd },
        { 0x30, 0, 0, 0x00000014 },
        { 0x15, 0, 11, 0x00000006 },
        { 0x28, 0, 0, 0x00000038 },
        { 0x15, 8, 9, 0x0000969b },
        { 0x15, 0, 8, 0x00000800 },
        { 0x30, 0, 0, 0x00000017 },
        { 0x15, 0, 6, 0x00000006 },
        { 0x28, 0, 0, 0x00000014 },
        { 0x45, 4, 0, 0x00001fff },
        { 0xb1, 0, 0, 0x0000000e },
        { 0x48, 0, 0, 0x00000010 },
        { 0x15, 0, 1, 0x0000969b },
        { 0x6, 0, 0, 0x00040000 },
        { 0x6, 0, 0, 0x00000000 },
    };
    
    if(family == AF_INET){
        fd = socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
        if(fd>=0)
            setsockopt(fd, IPPROTO_IP, IP_HDRINCL, &flag, sizeof(flag));
    }else if(family == AF_INET6){
        fd = socket(AF_INET6, SOCK_RAW, IPPROTO_TCP);
        if(fd>=0)
            setsockopt(fd, IPPROTO_IPV6, IP_HDRINCL, &flag, sizeof(flag));
    }
    struct sock_fprog bpf = {
        .len = sizeof(code)/sizeof(code[0]),
        .filter = code,
    };
    //int flags = fcntl(fd, F_GETFL);
    //fcntl(fd,F_SETFL,flags | O_NONBLOCK);
    //if(setsockopt(fd, SOL_SOCKET, SO_ATTACH_FILTER, &bpf, sizeof(bpf))<0)
    if(0)
    {
        close(fd);
        return -1;
    }
    return fd;
}

static void sndPacket(int fd, const char *pkt, quint16 len, quint32 ipv4)
{
    struct sockaddr_in v4;
    memset(&v4, 0, sizeof(v4));
    v4.sin_family = AF_INET;
    v4.sin_port = 0;
    v4.sin_addr.s_addr =ipv4;
    sendto(fd, pkt, len, 0, (const struct sockaddr *)&v4, sizeof(v4));
}

static int fd;
void *snd(void *arg)
{
    while(1){
        int nbytes = read(fd, buff, sizeof(buff));
        
        if(nbytes<=0){
            continue;
        }
        if((buff[0]>>4)!= 4) continue; //drop non-ipv4
        struct tcphdr *tcp = (struct tcphdr*)&buff[20];
        if(tcp->dest != htons(local_port) || tcp->rst) continue;
        printf("%s:%d\n", inet_ntoa(*(struct in_addr*)&buff[12]), ntohs(*(quint16*)&buff[20]));
    }
    return NULL;
}
//./scan 192.168.1.22 50.1.0.0/16 22
int main(int argc, char **argv)
{
    if(argc != 4) return -1;
               fd = create_ip_raw_socket(AF_INET);
    pthread_t thr;
    pthread_create(&thr, NULL, snd, NULL);
    char *c = strchr(argv[2], '/');
    quint32 count = 1;
    if(c){
        count = 1U << (32 - atoi(c+1) % 32);
        *c = '\0';
    }
    quint32 localIp_BE, remoteIp_BE;
    inet_pton(AF_INET, argv[1], &localIp_BE);
    inet_pton(AF_INET, argv[2], &remoteIp_BE);
    quint16 port = atoi(argv[3]);
    
    for(quint32 i = 1; i < count; ++i){
        quint32 r = htonl((ntohl(remoteIp_BE)+i));
        build_syn(buff, localIp_BE, r, port);
        sndPacket(fd, buff, sizeof(buff), r);
        if(i%32768 == 0) sleep(2);
    }
    
    sleep(5);
    
    
}
